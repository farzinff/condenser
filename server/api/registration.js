import koa_router from 'koa-router';
import koa_body from 'koa-body';
import config from 'config';
import models from 'db/models';
import { checkCSRF, getRemoteIp, rateLimitReq, rateLimitReqForSMS } from 'server/utils/misc';
import { hash } from 'golos-js/lib/auth/ecc';
import secureRandom from "secure-random";
import tt from 'counterpart';
import sendEmail from 'server/sendEmail';
import twilio from 'server/utils/twilio';
import requestManager from 'server/utils/requestManager';
import rp from 'request-promise';
import request from 'request';

function digits(text) {
    const digitArray = text.match(/\d+/g);
    return digitArray ? digitArray.join("") : "";
}

function sendPhoneCode(country, phone, code) {

    const number = country + phone;
    twilio(number, code);

}

/**
 * return status types:
 * session - new user without identity in DB
 * waiting - user verification phone in progress
 * done - user verification phone is successfuly done
 * already_used -
 * attempts_10 - Confirmation was attempted a moment ago. You can try again only in 10 seconds
 * attempts_300 - Confirmation was attempted a moment ago. You can try again only in 5 minutes
 * @param {*} app
 */
export default function useRegistrationApi(app) {
    const router = koa_router({ prefix: '/api/v1' });
    app.use(router.routes());
    const koaBody = koa_body();

    router.post("/verify_code", koaBody, function*() {
        if (!this.request.body) return;

        const accountSid = this.request.body && this.request.body.AccountSid ?
            this.request.body.AccountSid :
            '';
        if (accountSid.localeCompare(config.get('twilio.account_sid')) != 0) return;

        const phone = this.request.body && this.request.body.From ?
            this.request.body.From.substr(1) :
            '';
        if (!phone || digits(phone).length === 0) return;

        const confirmation_code = this.request.body.Body ?
            this.request.body.Body.substr(0, 4) :
            '';
        if (!confirmation_code || digits(confirmation_code).length !== 4) return;

        const phoneHash = hash.sha256(phone, 'hex');

        console.log(
            "-- /api/v1/confirm_provider -->",
            phone,
            confirmation_code
        );

        let mid = yield models.Identity.findOne({
            attributes: ["id", "user_id", "verified", "updated_at", "phone"],
            where: {
                phone: phoneHash,
                confirmation_code,
                provider: "phone"
            },
            order: "id DESC"
        });
        if (!mid) {
            this.status = 401;
            this.body = "Wrong confirmation code";
            return;
        }
        if (mid.verified) {
            this.status = 401;
            this.body = "Phone number has already been verified";
            return;
        }

        const hours_ago = (Date.now() - mid.updated_at) / 1000.0 / 3600.0;
        if (hours_ago > 24.0) {
            this.status = 401;
            this.body = "Confirmation code has been expired";
            return;
        }
        yield mid.update({ verified: true });
        this.body = "INSTACHAIN.io \nThank you for confirming your phone number";
    });

    router.post("/check_code", koaBody, function*() {
        if (rateLimitReq(this, this.req)) return;
        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        this.body = JSON.stringify({ status: "session" });

        let user_id = this.session.user;
        if (!user_id) {
            return;
        }

        let mid = yield models.Identity.findOne({
            attributes: ["id", "phone", "verified", "updated_at", "confirmation_code"],
            where: { user_id, provider: "phone" },
            order: "id DESC"
        });
        if (mid) {
            if (mid.verified) {
                this.body = JSON.stringify({ status: "done" });
                return;
            }
            this.body = JSON.stringify({ status: "waiting", code: mid.confirmation_code });
            const seconds_ago = (Date.now() - mid.updated_at) / 1000.0;
            const timeAgo = 10;
            if (seconds_ago < timeAgo) {
                this.body = JSON.stringify({ status: "attempts_10" });
                return;
            }
        }
    });


    router.post("/send_email_code", koaBody, function*() {
        if (rateLimitReqForSMS(this, this.req, 86400, "send_email_code")) return;
        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        const email = params.email ? params.email : null;

        console.log(params);

        if (!email || email === "") {
            this.body = JSON.stringify({ status: "provide_email" });
            return;
        }

        const existing_email = yield models.User.findOne({
            attributes: ["email"],
            where: { email: email },
        });

        if (existing_email) {
            this.body = JSON.stringify({ status: "email_already_used" });
            return;
        }

        const confirmation_code = parseInt(secureRandom.randomBuffer(8).toString("hex"), 16).toString(10).substring(0, 4); // 4 digit code

        const html = "Thank you for joining INSTACHAIN. This is your confirmation code: " + confirmation_code;

        sendEmail('INSTACHAIN CONFIRMATION', html, email);


        console.log('confirmation_code : ', confirmation_code);

        let mid = yield models.UserValidation.findOne({
            attributes: ["email", "email_code", "email_verified", "updated_at"],
            where: { "email": email },
        });



        if (mid) {

            yield models.UserValidation.update({
                email_code: confirmation_code,
                phone_code: '',
                email_verified: false,
                phone_verified: false
            }, { where: { email: email } });

            //  yield mid.update({email_code: confirmation_code, email_verified: false, phone: '', phone_verified: false });

        } else {

            mid = yield models.UserValidation.create({
                email: email,
                email_code: confirmation_code,
                email_verified: false,
                phone: '',
                phone_verified: false
            });
        }

        this.body = JSON.stringify({ status: "email_code_waiting" });
        // this.body = JSON.stringify({ status: "done" }); // DISABLE PHONE VERIFICATION
    });


    router.post("/verify_email_code", koaBody, function*() {
        if (rateLimitReq(this, this.req)) return;
        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        const email = params.email ? params.email : null;
        const email_code = params.email_code ? params.email_code : null;

        console.log(params);

        if (!email || email === "") {
            this.body = JSON.stringify({ status: "provide_email" });
            return;
        }

        if (!email_code || email_code === "") {
            this.body = JSON.stringify({ status: "provide_email_code" });
            return;
        }

        const existing_email = yield models.User.findOne({
            attributes: ["email"],
            where: { email: email },
        });

        if (existing_email) {
            this.body = JSON.stringify({ status: "email_already_used" });
            return;
        }

        let mid = yield models.UserValidation.findOne({
            attributes: ["email", "email_code", "email_verified", "updated_at"],
            where: { "email": email, "email_code": email_code },
            order: "email DESC"
        });

        if (mid) {

            yield models.UserValidation.update({
                email_verified: true
            }, { where: { email: email, "email_code": email_code } });

            //  yield mid.update({email_verified: true});
            this.body = JSON.stringify({ status: "email_code_verified" });
        } else {
            this.body = JSON.stringify({ status: "email_code_false" });
        }
    });


    router.post("/send_phone_code", koaBody, function*() {
        // if (rateLimitReq(this, this.req)) return;
        if (rateLimitReqForSMS(this, this.req, 86400, "send_phone_code")) return;

        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        const email = params.email ? params.email : null;
        const email_code = params.email_code ? params.email_code : null;
        var phone = params.phone ? params.phone : null;
        const country = params.country ? params.country : null;

        console.log(params);

        if (!email || email === "") {
            this.body = JSON.stringify({ status: "provide_email" });
            return;
        }

        if (!email_code || email_code === "") {
            this.body = JSON.stringify({ status: "provide_email_code" });
            return;
        }

        if (!country || country === "") {
            this.body = JSON.stringify({ status: "select_country" });
            return;
        }

        if (!phone || phone === "") {
            this.body = JSON.stringify({ status: "provide_phone" });
            return;
        }

        const existing_email = yield models.User.findOne({
            attributes: ["email"],
            where: { email: email },
        });

        if (existing_email) {
            this.body = JSON.stringify({ status: "email_already_used" });
            return;
        }

        const phoneh = digits(parseInt(country) + phone);
        const phoneHash = hash.sha256(phoneh, 'hex')
        const existing_phone = yield models.Identity.findOne({
            attributes: ["user_id"],
            where: { phone: phoneHash, provider: "phone", verified: true },
            order: "id DESC"
        });

        let user_id = this.session.user;

        if (existing_phone && existing_phone.user_id != user_id) {
            console.log(
                "-- /send_code existing_phone -->",
                user_id,
                this.session.uid,
                phoneHash,
                existing_phone.user_id
            );
            this.body = JSON.stringify({ status: "already_used" });
            return;
        }

        phone = phone.toString();

        const confirmation_code = parseInt(secureRandom.randomBuffer(8).toString("hex"), 16).toString(10).substring(0, 4); // 4 digit code

        console.log('mobile confirmation_code : ', confirmation_code);

        let mid = yield models.UserValidation.findOne({
            attributes: ["email", "email_code", "email_verified", "updated_at"],
            where: { "email": email, "email_code": email_code, "email_verified": true },
        });


        if (mid) {

            yield models.UserValidation.update({
                phone: phone,
                country: country,
                phone_code: confirmation_code,
                phone_verified: false
            }, { where: { "email": email, "email_code": email_code, "email_verified": true } });

            sendPhoneCode(country, phone, confirmation_code);
            this.body = JSON.stringify({ status: "phone_code_waiting" });
        } else {
            this.body = JSON.stringify({ status: "first_verify_email_code" });
        }
    });


    router.post("/verify_phone_code", koaBody, function*() {
        if (rateLimitReq(this, this.req)) return;
        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        const email = params.email ? params.email : null;
        const email_code = params.email_code ? params.email_code : null;
        const country = params.country ? params.country : null;
        const localPhone = params.phone ? params.phone : null;
        const phone_code = params.phone ? params.phone_code : null;
        const retry = params.retry ? params.retry : null;
        console.log(params);

        if (!email || email === "") {
            this.body = JSON.stringify({ status: "provide_email" });
            return;
        }

        if (!email_code || email_code === "") {
            this.body = JSON.stringify({ status: "provide_email_code" });
            return;
        }


        if (!phone_code || phone_code === "") {
            this.body = JSON.stringify({ status: "provide_phone_code" });
            return;
        }

        if (!country || country === "") {
            this.body = JSON.stringify({ status: "select_country" });
            return;
        }

        if (!localPhone || digits(localPhone).length === 0) {
            this.body = JSON.stringify({ status: "provide_phone" });
            return;
        }

        const phone = digits(parseInt(country) + localPhone);
        const phoneHash = hash.sha256(phone, 'hex')
        const existing_phone = yield models.Identity.findOne({
            attributes: ["user_id"],
            where: { phone: phoneHash, provider: "phone", verified: true },
            order: "id DESC"
        });

        let user_id = this.session.user;
        if (existing_phone && existing_phone.user_id != user_id) {
            console.log(
                "-- /send_code existing_phone -->",
                user_id,
                this.session.uid,
                phoneHash,
                existing_phone.user_id
            );
            this.body = JSON.stringify({ status: "already_used" });
            return;
        }

        let mid = yield models.Identity.findOne({
            attributes: ["id", "phone", "verified", "updated_at", "confirmation_code"],
            where: { user_id, provider: "phone" },
            order: "id DESC"
        });

        let mid2 = yield models.UserValidation.findOne({
            attributes: ["email", "email_code", "email_verified", "updated_at"],
            where: { "email": email, "email_code": email_code, "email_verified": true, "phone": localPhone, "phone_code": phone_code },
        });

        if (mid2) {

            yield models.UserValidation.update({ phone_verified: true }, { where: { "email": email, "email_code": email_code, "email_verified": true, "phone": localPhone, "phone_code": phone_code } });

            var remoteIp = getRemoteIp(this.request.req);

            console.log('************ this.session.uid : ************  ', this.session.uid);

            

            try{
              var user = yield models.User.create({
                uid: this.session.uid,
                remote_ip: remoteIp
            });

            this.session.user = user_id = user.id;

              yield models.Identity.create({
                provider: "phone",
                user_id,
                uid: this.session.uid,
                email: email,
                phone_no: localPhone.toString(),
                country_code: country.toString(),
                phone: phoneHash,
                verified: true, // DISABLE PHONE VERIFICATION
                phone_code
            });
            }catch(e){

              console.log('Identity create error : ',e);
              this.throw(e);
            }

            
            this.body = JSON.stringify({ status: "done" });
        } else {
            this.body = JSON.stringify({ status: "wrong_phone_code" });
        }

    });

    router.get("/users_list", koaBody, function*() {


        let token = this.request.header.authorization || '0';

        console.log('this.request : ',this.request.query);

        let limit = Number(this.request.query.perPage) || 10;   // number of records per page
        let offset = (Number(this.request.query.page) || 0) * (Number(this.request.query.perPage) || 0);

        const options = {
            method: 'GET',
            uri: 'http://85.25.195.93:8010/admins/isAuthenticated',
            headers: {
                'Authorization': token
            },
            json: true
        };

        let adminData = {};

        try{
           adminData = yield rp(options);
        }catch(e){
            this.throw(e);
        }


        if (adminData.success) {

            let mid = yield models.Identity.findAll({
                attributes: ["id", "UserId", "email", "phone", "provider", "created_at","phone_no","country_code", "phone"],
                where: { verified: true },
                limit:limit,
                offset:offset,
                order: "id DESC"
            });
            this.body = mid;
        }
    });

    router.post("/send_code", koaBody, function*() {
        if (rateLimitReq(this, this.req)) return;
        const body = this.request.body;
        let params = {};
        if (typeof(body) === 'string') {
            try {
                params = JSON.parse(body)
            } catch (e) {}
        } else {
            params = body;
        }
        if (!checkCSRF(this, params.csrf)) return;

        const country = params.country ? params.country : null;
        const localPhone = params.phone ? params.phone : null;
        const retry = params.retry ? params.retry : null;
        console.log(params);

        if (!country || country === "") {
            this.body = JSON.stringify({ status: "select_country" });
            return;
        }

        if (!localPhone || digits(localPhone).length === 0) {
            this.body = JSON.stringify({ status: "provide_phone" });
            return;
        }

        const phone = digits(parseInt(country) + localPhone);
        const phoneHash = hash.sha256(phone, 'hex')
        const existing_phone = yield models.Identity.findOne({
            attributes: ["user_id"],
            where: { phone: phoneHash, provider: "phone", verified: true },
            order: "id DESC"
        });

        let user_id = this.session.user;
        if (existing_phone && existing_phone.user_id != user_id) {
            console.log(
                "-- /send_code existing_phone -->",
                user_id,
                this.session.uid,
                phoneHash,
                existing_phone.user_id
            );
            this.body = JSON.stringify({ status: "already_used" });
            return;
        }

        let confirmation_code = parseInt(secureRandom.randomBuffer(8).toString("hex"), 16).toString(10).substring(0, 4); // 4 digit code
        let mid = yield models.Identity.findOne({
            attributes: ["id", "phone", "verified", "updated_at", "confirmation_code"],
            where: { user_id, provider: "phone" },
            order: "id DESC"
        });
        if (mid) {
            if (mid.verified) {
                if (mid.phone === phoneHash) {
                    this.body = JSON.stringify({ status: "done" });
                    return;
                }
                yield mid.update({ verified: false, phone: phoneHash });
            }
            const seconds_ago = (Date.now() - mid.updated_at) / 1000.0;
            const timeAgo = process.env.NODE_ENV === "production" ? 300 : 10;
            if (retry) {
                confirmation_code = mid.confirmation_code
            } else {
                if (seconds_ago < timeAgo) {
                    this.body = JSON.stringify({ status: "attempts_300" });
                    return;
                }
                yield mid.update({ confirmation_code, phone: phoneHash });
            }
        } else {
            let user;
            if (user_id) {
                user = yield models.User.findOne({
                    attributes: ["id"],
                    where: { id: user_id }
                });
            }

            console.log('getRemoteIp(this.request.req) : ', getRemoteIp(this.request.req))


            if (!user) {
                user = yield models.User.create({
                    uid: this.session.uid,
                    remote_ip: getRemoteIp(this.request.req)
                    // remote_ip: '127.0.0.12'
                });
                this.session.user = user_id = user.id;
            }
            mid = yield models.Identity.create({
                provider: "phone",
                user_id,
                uid: this.session.uid,
                phone: phoneHash,
                verified: false,
                // verified: true, // DISABLE PHONE VERIFICATION
                confirmation_code
            });
        }
        this.body = JSON.stringify({ status: "waiting", code: confirmation_code });
        // this.body = JSON.stringify({ status: "done" }); // DISABLE PHONE VERIFICATION
    });
}
